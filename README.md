# Rust Web App Example
### using [Dioxus](https://dioxuslabs.com), [Axum](https://github.com/tokio-rs/axum) and [sqlx](https://github.com/launchbadge/sqlx) with dockerized dev env 

1. open project folder in vscode
2. make sure vscode docker extension is installed
3. right click `docker-compose.yml` file and select `Compose Up` and wait until the containers are built
4. click docker extension icon and then under `Containers` tab then right click `rust-web-app-dev` and select `Attach Visual Studio Code`
5. click button `Open Folder` and select folder `../workspace`
6. cd to `db` and run `sqlx migrate run`
7. restart the `rust-web-app-app` container, or edit the code, to reload the app
8. [view app](http://localhost:4300)
