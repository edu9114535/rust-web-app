use {
    serde::{Deserialize, Serialize},
    std::hash::{Hash, Hasher}
};

pub use rand;
pub use chrono;
pub use dotenv;
pub use anyhow;
pub use once_cell;
pub use uuid::Uuid;

pub mod message {
    use super::*;

    #[derive(Serialize, Deserialize)]
    pub struct Create {
        pub body: String,
    }

    #[derive(Serialize, Deserialize, Clone)]
    pub struct Model {
        pub id: uuid::Uuid,
        pub body: String,
    }

    impl Hash for Model {
        fn hash<H: Hasher>(&self, state: &mut H) {
            self.id.hash(state);
        }
    }

    impl PartialEq for Model {
        fn eq(&self, other: &Self) -> bool {
            self.id == other.id
        }
    }
    
    impl Eq for Model {}
}
