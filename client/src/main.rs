#![allow(non_snake_case)]

use {
    shared::{
        chrono,
        Uuid,
        message::Model as MessageModel,
        message::Create as MessageCreateModel
    },
    dioxus::prelude::*,
    dioxus_router::prelude::*,
    gloo_net::http::Request,
    gloo_timers::future::TimeoutFuture,
    futures_util::StreamExt
};

#[derive(Routable, Clone)]
#[rustfmt::skip]
enum Route {
    #[layout(Nav)]
        #[route("/")]
        Home {},
        #[route("/message")]
        Message {},
}

#[inline_props]
fn Home(cx: Scope) -> Element {
    render! {
        div {
            class: "container py-3",
            div {
                class: "row",
                div { 
                    class: "col-12",
                    div {
                        class: "card shadow",
                        div {
                            class: "row g-0",
                            div {
                                class: "col-4",
                                img {
                                    class: "img-fluid rounded-start",
                                    src: "public/learn.png"
                                }
                            }
                            div {
                                class: "col-8",
                                div {
                                    class: "card-body",
                                    h5 {
                                        class: "card-title",
                                        "Rust Web App Example"
                                    }
                                    p {
                                        class: "card-text",
                                        "Sample project using Dioxus with Axum & Postgres in Docker, to get you started!"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

#[inline_props]
fn Message(cx: Scope) -> Element {
    let messages: &UseRef<Vec<MessageModel>> = use_ref(cx, Vec::new);

    use_future(cx, (), |_|  {
        to_owned![messages];
        async move {
            if let Ok(response) = Request::get("/api/messages").send().await {
                if let Ok(data) = response.json::<Vec<MessageModel>>().await {
                    messages.with_mut(|m| m.extend(data))
                }
            }
        } 
    });

    let delete = use_coroutine(cx, |mut rx: UnboundedReceiver<Uuid>| {
        to_owned![messages];
        async move {
            while let Some(id) = rx.next().await {
                if id.is_nil() {
                    continue;
                }

                if Request::delete(&format!("/api/messages/{id}")).send().await.is_ok() {
                    messages.with_mut(|s| s.retain(|k| k.id != id))
                }
            }
        }
    });

    let create = use_coroutine(cx, |mut rx: UnboundedReceiver<String>| {
        to_owned![messages];
        async move {
            while let Some(body) = rx.next().await {
                if body.is_empty() {
                    continue;
                }

                if let Ok(response) = Request::post("/api/messages")
                    .json(&MessageCreateModel { body: body.to_string() })
                    .unwrap()
                    .send()
                    .await {
                    if let Some(location) = response.headers().get("location") {
                        if let Ok(response) = Request::get(&location).send().await {
                            if let Ok(data) = response.json::<MessageModel>().await {
                                messages.with_mut(|s| s.push(data))
                            }
                        }
                    }
                }
            }
        }
    });
    
    render! {
        div {
            class: "container",
            div {
                class: "row py-3",
                div {
                    class: "col-12",
                    h3 { 
                        "Messages"
                    }
                }
                div {
                    class: "col-12",
                    ul {
                        class: "list-group",
                        for message in messages.with(|s| s.clone()) {
                            render! {
                                li {
                                    class:  "list-group-item",
                                    div {
                                        class: "row justify-content-between",
                                        div {
                                            class: "col-auto",
                                            p {
                                                class: "text-truncate",
                                                "{message.body}",
                                            }
                                        }
                                        div {
                                            class: "col-auto",
                                            button {
                                                class: "btn btn-danger",
                                                onclick: move |_| delete.send(message.id),
                                                i {
                                                    class: "fa-solid fa-trash fa-lg",
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            form {
                onsubmit: move |ev| create.send(ev.values["body"][0].clone()),
                div {
                    class: "row justify-content-between",
                    div {
                        class: "col-10",
                        input {
                            r#type: "text",
                            name: "body",
                            class: "form-control"
                        }
                    }
                    div {
                        class: "col-2",
                        button { 
                            class: "btn btn-primary float-end",
                            r#type: "submit",
                            "Create"
                        }
                    }
                }
            }
        }
    }
}

fn Nav(cx: Scope) -> Element {
    let now = use_state(cx, chrono::Local::now);

    use_future(cx, (), move |_| {
        to_owned![now];
        async move {
            loop {
                TimeoutFuture::new(30_000).await;
                now.set(chrono::Local::now());
            }
        }
    });
    
    render! {
        nav {
            class: "navbar navbar-expand-lg bg-body-secondary",
            "data-bs-theme": "dark",
            div {
                class: "container-fluid",
                span {
                    class: "navbar-brand",
                    i {
                        class: "fa-brands fa-rust fa-lg",
                    }
                }
                button {
                    class: "navbar-toggler",
                    r#type: "button",
                    "data-bs-toggle": "collapse",
                    "data-bs-target": "#navbarNav",
                    "aria-controls": "navbarNav",
                    "aria-expanded": "false",
                    "aria-label": "Toggle navigation",
                    span {
                        class: "navbar-toggler-icon"
                    }
                }
                div {
                    class: "collapse navbar-collapse",
                    id: "navbarNav",
                    ul {
                        class: "navbar-nav me-auto",
                        li { 
                            class: "nav-item",
                            Link {
                                class: "nav-link",
                                to: Route::Home {},
                                "Home"
                            }
                        }
                        li { 
                            class: "nav-item",
                            Link {
                                class: "nav-link",
                                to: "/message",
                                "Messages"
                            }
                        }
                    }
                    div {
                        class: "d-flex",
                        style: "cursor:pointer;",
                        title: "{now:?}",
                        span {
                            class: "navbar-text text-secondary",
                            now.format("%H:%M").to_string(),
                            i {
                                class: "ps-1 fa-solid fa-clock"
                            }
                        }
                    }
                }
            }
        }
        Outlet::<Route> {}
    }
}

pub fn main() {
    dioxus_web::launch(|cx| render!(Router::<Route> {}));
}