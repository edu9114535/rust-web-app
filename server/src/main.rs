use {
    std::net::SocketAddr,
    axum::{
        routing::{
            get,
            post,
            delete
        },
        extract::Path,
        http::{StatusCode, header},
        Json,
        Router,
        response::IntoResponse
    },
    shared::{
        anyhow::{Result, Context},
        dotenv,
        Uuid,
        message::{
            Model as MessageModel,
            Create as MessageCreateModel
        }
    }
};

type WebResponse<T> = (StatusCode, T);
type HandlerResult<T> = Result<WebResponse<T>, WebResponse<String>>;

#[tokio::main]
async fn main() -> Result<()> {
    db::init(
        &dotenv::var("DATABASE_URL").expect("db env var not set"),
        1,
        1,
        30,
        300
    ).await;
    
    let app = Router::new()
        .route("/", get(root))
        .route("/messages", get(get_messages))
        .route("/messages/:id", get(find_message))
        .route("/messages/:id", delete(delete_message))
        .route("/messages", post(create_message));

    let addr: SocketAddr = format!(
            "{}:{}",
            dotenv::var("SERVER_HOST").expect("host env var not set"),
            dotenv::var("SERVER_PORT").expect("port env var not set")
        )
        .parse()
        .expect("invalid app address");

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .context("web server failed to start")
}

async fn root() -> &'static str {
    "rust backend using axum"
}

async fn get_messages() -> HandlerResult<Json<Vec<MessageModel>>> {
    match db::message::list().await {
        Ok(messages) => Ok((StatusCode::OK, Json(messages))),
        Err(e) => Err((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()))
    }
}

async fn find_message(Path(id): Path<Uuid>) -> HandlerResult<Json<MessageModel>> {
    match db::message::find(&id).await {
        Ok(message) => Ok((StatusCode::OK, Json(message))),
        Err(e) => Err((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()))
    }
}

async fn delete_message(Path(id): Path<Uuid>) -> HandlerResult<()> {
    match db::message::delete(&id).await {
        Ok(_) => Ok((StatusCode::NO_CONTENT, ())),
        Err(e) => Err((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()))
    }
}

async fn create_message(Json(payload): Json<MessageCreateModel>) -> Result<impl IntoResponse, WebResponse<String>> {
    match db::message::insert(&payload.body).await {
        Ok(id) => 
            Ok((
                StatusCode::CREATED,
                [(header::LOCATION, format!("/api/messages/{id}"))]
            )),
        Err(e) => Err((StatusCode::INTERNAL_SERVER_ERROR, e.to_string()))
    }
}