use {
    shared::once_cell::sync::OnceCell,
    sqlx::postgres::PgPoolOptions,
    std::time::Duration,
};

pub mod message;

type PgPool = sqlx::Pool<sqlx::Postgres>;

static PG_POOL: OnceCell<PgPool> = OnceCell::new();

pub(crate) fn get_pool() -> &'static PgPool {
    PG_POOL.get().expect("db pool not initialized")
}

pub async fn init(url: &str, min_connections: u32, max_connections: u32, idle: u64, lifetime: u64) {
    let pool = PgPoolOptions::new()
        .min_connections(min_connections)
        .max_connections(max_connections)
        .idle_timeout(Duration::new(idle, 0))
        .max_lifetime(Duration::new(lifetime, 0))
        .acquire_timeout(Duration::new(lifetime, 0))
        .connect(url)
        .await
        .expect("failed to create pg pool");

    PG_POOL
        .set(pool)
        .expect("failed to create pg pool cell");
}