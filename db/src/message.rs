use {
    crate::get_pool,
    shared::anyhow::Result,
    sqlx::types::Uuid,
    shared::message::Model as MessageModel
};

pub async fn find(message_id: &Uuid) -> Result<MessageModel> {
    let row = sqlx::query!(
        r#"
        SELECT message_id, body
        FROM message
        WHERE message_id = $1
        "#,
        message_id
    )
    .fetch_one(get_pool())
    .await?;

    Ok(MessageModel { id: row.message_id, body: row.body })
}

pub async fn insert(body: &str) -> Result<Uuid> {
    let message_id = sqlx::query!(
        r#"
        INSERT INTO message (body)
        VALUES ($1)
        RETURNING message_id
        "#,
        body
    )
    .fetch_one(get_pool())
    .await?
    .message_id;

    Ok(message_id)
}

pub async fn delete(message_id: &Uuid) -> Result<()> {
    sqlx::query!(
        r#"
        DELETE FROM message
        WHERE message_id=$1
        "#,
        message_id
    )
    .execute(get_pool())
    .await?;

    Ok(())
}

pub async fn list() -> Result<Vec<MessageModel>> {
    let rows = sqlx::query!(
        r#"
        SELECT message_id, body
        FROM message
        ORDER BY created_at
        "#
    )
    .fetch_all(get_pool())
    .await?;

    let messages = rows
        .iter()
        .map(|r| MessageModel { id: r.message_id, body: r.body.to_owned() })
        .collect();

    Ok(messages)
}