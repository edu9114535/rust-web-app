CREATE OR REPLACE FUNCTION unix_ticks()
    RETURNS INT8
    LANGUAGE plpgsql
AS $$
BEGIN
RETURN(SELECT EXTRACT(EPOCH FROM NOW() AT TIME ZONE 'UTC')::INT8);
END $$;

CREATE TABLE IF NOT EXISTS message (
    message_id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    created_at INT8 NOT NULL DEFAULT unix_ticks(),
    body TEXT NOT NULL
);

INSERT INTO message (
    body
)
VALUES (
    'hello world!'
);